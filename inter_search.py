from random import randint
def greedy_random(target, set):
    greedyset = set
    total = 0
    j = 0
    tempset = []
 
    while (len(greedyset) != 0):
        r = randint(0, (len(set) - 1))
 
        total += greedyset[r]
        tempset.append(greedyset[r])
        if total > target:
            total -= greedyset[r]
            tempset.pop()
        greedyset.pop(r)
 
        j += 1
    return tempset
 
def inter_search(target, theset):
    best = 0
    setyset = theset
    isAnnealed = False
 
    while (isAnnealed != True):
        greedy = greedy_random(target, setyset[:])
        greedy.sort()
        sumgreedy = sum(greedy)
        if sumgreedy > best and sumgreedy <= target:
            best = sumgreedy
        print greedy
        print best
        if (len(greedy) == 0):
            break
        thing = greedy[(len(greedy) - 1)]
        addyadd = target
        print setyset
        setyset.pop()
        
        if len(setyset) == 0:
            isAnnealed = True
    return best
 
print inter_search(12,[3,2,3,4,5,2,1])