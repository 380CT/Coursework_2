from random import randint
from random import sample

def randSet(num,numbs):
    x = [randint(1,numbs) for p in range(0,num)]
    y = sum(sample(x, randint(1,len(x))))
    return x,y


testset = randSet(10,20)

print testset
