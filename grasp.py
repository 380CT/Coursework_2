from random import randint
def greedy_random(target, set):
    greedyset = set
    total = 0
    j = 0
    tempset = []

    while (len(greedyset) != 0):
        r = randint(0, (len(set) - 1))

        total += greedyset[r]
        tempset.append(greedyset[r])
        if total > target:
            total -= greedyset[r]
            tempset.pop()
        greedyset.pop(r)

        j += 1
    return (total)

#greedy(9, [3, 34, 47676677, 12, 3, 2])




def grasp(target,set):

    best = target
    term = 0

    while (term <100):
        greedy = greedy_random(target,set)
        if target - greedy < best:
            best = greedy
        term += 1
    print best


grasp(15, [3, 34, 47676677, 12, 3, 4, 3, 4, 5, 6, 7, 8, 3])
