
def subsetsum(numbers, target):
   size = 1
   subsets = []
   while size <= len(numbers):
      array = combinations(numbers, size)
      for combination in array:
          if sum(combination) == target:
            if combination not in subsets:
             subsets.append(combination)
      size += 1
   return subsets


def combinations(numbers, size):
    results = []
    if len(numbers) <= 0 or size <= 0:
       results.append([])
       return results
    else:
      for i in range(len(numbers)):
          number = numbers[i]
          for combination in combinations(numbers[+1:], size-1):
              results.append([number]+ combination)
    return results




#print(subsetsum([1,2,3,4,5], 17))

print(subsetsum([1,2,3,4,5,6,7,8,9], 16))



def subset(set, target):
    combinations = []
    for i in set:
        combinations.append([i])
    return combinations


# print (subset([2,4,6],3))
